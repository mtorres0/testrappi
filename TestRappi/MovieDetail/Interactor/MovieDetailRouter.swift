//
//  MovieDetailRouter.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation
import UIKit

class MovieDetailRouter {
    static func createMovieDetailViewController(movie: Movie) -> UIViewController {
        // Router
        let router = MovieDetailRouter()
        // Entities
        guard let urlMovies = URL(string: ApiConstants.baseURL) else { return UIViewController() }
        let parameters = [
            ParameterName.apiKey: ApiConstants.apiKey
        ]
        let movieVideosServices = GetMovieVideosServices(baseURL: urlMovies, parameters: parameters)
        // Interactor
        let interactor = MovieDetailInteractor(movieVideosServices: movieVideosServices)
        // Presenter
        let presenter = MovieDetailPresenter(interactor: interactor, router: router)
        // View
        let view = MovieDetailViewController(movie: movie, presenter: presenter)
        presenter.viewable = view
        interactor.interactorOutput = presenter
        return view
    }
}
