//
//  MovieDetailPresenter.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

protocol MovieDetailViewable: NSObjectProtocol {
    func showVideos()
}

class MovieDetailPresenter: NSObject {
    weak var viewable: MovieDetailViewable?
    private let interactor: MovieDetailInteractor
    private let router: MovieDetailRouter
    var videos: [MovieVideo]? {
        didSet{
            viewable?.showVideos()
        }
    }
    
    init(interactor: MovieDetailInteractor, router: MovieDetailRouter) {
        self.interactor = interactor
        self.router = router
    }
    
    func getVideosFor(movieId: Int) {
        interactor.getVideosFromService(movieId: movieId)
    }
}

extension MovieDetailPresenter: MovieDetailInteractorOutput {
    func videosList(videos: [MovieVideo]) {
        self.videos = videos
    }
}
