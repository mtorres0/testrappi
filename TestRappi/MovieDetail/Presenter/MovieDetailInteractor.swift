//
//  MovieDetailInteractor.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

protocol MovieDetailInteractorOutput: NSObjectProtocol {
    func videosList(videos: [MovieVideo])
}

class MovieDetailInteractor {
    private let movieVideosServices: GetMovieVideosServices
    weak var interactorOutput: MovieDetailInteractorOutput?
    
    init(movieVideosServices: GetMovieVideosServices) {
        self.movieVideosServices = movieVideosServices
    }
    
    func getVideosFromService(movieId: Int) {
        movieVideosServices.getMovies(movieId: movieId) { result in
            switch result {
            case .success(let videosSuccessResponse):
                self.interactorOutput?.videosList(videos: videosSuccessResponse.videos)
            case .failure(let error):
                debugPrint("Get Movie Videos Services: \(error.localizedDescription)")
            }
        }
    }
}
