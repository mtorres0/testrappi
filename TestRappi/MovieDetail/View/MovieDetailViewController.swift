//
//  MovieDetailViewController.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 16/07/21.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    private let presenter: MovieDetailPresenter
    private let movie: Movie
    
    init(movie: Movie, presenter: MovieDetailPresenter) {
        self.movie = movie
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(movie: movie)
        setupCollectionView()
        presenter.getVideosFor(movieId: movie.id)
    }
    
    private func setup(movie: Movie) {
        movieImageView.imageFromURL(
            urlString: "\(ApiConstants.baseImageURL)\(movie.imagePath)",
            placeholder: UIImage(systemName: "play.rectangle.fill") ?? UIImage()
        )
        movieTitleLabel.text = movie.title
        releaseDateLabel.text = movie.releaseDate
        overviewTextView.text = movie.overview
    }
    
    private func setupCollectionView() {
        videosCollectionView.delegate = self
        videosCollectionView.dataSource = self
        videosCollectionView.register(UINib(nibName: "VideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "videoCell")
    }

}

extension MovieDetailViewController: MovieDetailViewable {
    func showVideos() {
        print("Number of videos: \(presenter.videos?.count ?? 0)")
        DispatchQueue.main.async {
            self.videosCollectionView.reloadData()
        }
    }
}

extension MovieDetailViewController: MovieTransitionable {
    var backgroundView: UIView {
        return self.backgroundView
    }

    var movieImage: UIImageView {
        return self.movieImageView
    }
}

extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.videos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! VideoCollectionViewCell
        if let video = presenter.videos?[indexPath.row] {
            cell.setup(video: video)
        }
        return cell
    }
}
