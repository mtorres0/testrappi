//
//  VideoCollectionViewCell.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import UIKit
import youtube_ios_player_helper

class VideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(video: MovieVideo) {
        videoTitleLabel.text = video.name
        playerView.load(withVideoId: video.key)
    }
    
}
