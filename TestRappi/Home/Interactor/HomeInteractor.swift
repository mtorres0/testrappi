//
//  HomeInteractor.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import Foundation

protocol HomeInteractorOutput: NSObjectProtocol {
    func moviesList(movies: [Movie])
    func handlerError(error: Error)
}

class HomeInteractor {
    private let moviesServices: GetMoviesServices
    weak var interactorOutput: HomeInteractorOutput?
    
    init(moviesServices: GetMoviesServices) {
        self.moviesServices = moviesServices
    }
    
    func requestMoviesFromService() {
        moviesServices.getMovies { result in
            switch result {
            case .success(let moviesSuccessResponse):
                self.interactorOutput?.moviesList(movies: moviesSuccessResponse.results)
            case .failure(let error):
                debugPrint("Error Get Movies Service: \(error.localizedDescription)")
                self.interactorOutput?.handlerError(error: error)
            }
        }
    }
}
