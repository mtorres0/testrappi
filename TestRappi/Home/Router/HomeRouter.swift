//
//  HomeRouter.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import Foundation
import UIKit

class HomeRouter {
    var navigationController: UINavigationController?
    
    static func createHomeViewController(category: MovieCategory) -> UIViewController {
        // router
        let router = HomeRouter()
        // entities
        guard let urlMovies = URL(string: ApiConstants.baseURL) else { return UIViewController() }
        let parameters = [
            ParameterName.apiKey: ApiConstants.apiKey
        ]
        let moviesServices = GetMoviesServices(baseURL: urlMovies, parameters: parameters, category: category)
        // view
        let view = HomeViewController()
        let navigationController = UINavigationController(rootViewController: view)
        // interactor
        let interactor = HomeInteractor(moviesServices: moviesServices)
        // presenter
        let presenter = HomePresenter(viewable: view, router: router, interactor: interactor)
        view.presenter = presenter
        interactor.interactorOutput = presenter
        router.navigationController = navigationController
        return navigationController
    }
    
    func goToMovieDetail(movie: Movie) {
        let viewController = MovieDetailRouter.createMovieDetailViewController(movie: movie)
        navigationController?.modalPresentationStyle = .custom
        navigationController?.pushViewController(viewController, animated: true)
    }
}
