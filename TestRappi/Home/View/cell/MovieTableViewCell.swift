//
//  MovieTableViewCell.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        movieImageView.image = nil
    }

    func setup(movie: Movie) {
        titleLabel.text = movie.title
        releaseDateLabel.text = movie.releaseDate
        overviewTextView.text = movie.overview
        movieImageView.imageFromURL(
            urlString: "\(ApiConstants.baseImageURL)\(movie.imagePath)",
            placeholder: UIImage(systemName: "play.rectangle.fill") ?? UIImage()
        )
    }
    
}
