//
//  HomeViewController.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import UIKit

class HomeViewController: UITableViewController {
    
    var presenter: HomePresenter?
    lazy var searchController: UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .default
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "search_a_movie".localized
        return controller
    })()
    
    lazy var activityIndicator: UIActivityIndicatorView = ({
        let activityView = UIActivityIndicatorView(style: .large)
        self.view.addSubview(activityView)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return activityView
    })()
    
    private var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        setupTableView()
        manageSearchBarController()
        activityIndicator.startAnimating()
        presenter?.getMovies()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
    }
    
    private func manageSearchBarController() {
        let searchBar = searchController.searchBar
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        tableView.tableHeaderView = searchBar
        tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.filteredMovies?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
        if let movie = presenter?.filteredMovies?[indexPath.row] {
            cell.setup(movie: movie)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        presenter?.goToMovieDetail(indexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 50, 0)
        cell.layer.transform = rotationTransform
        cell.alpha = 0

        UIView.animate(withDuration: 0.75) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1.0
        }
        
    }
}

extension HomeViewController: UISearchControllerDelegate, UISearchResultsUpdating {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.isActive = false
        showMovies()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        presenter?.filterMovies(title: text)
    }
}


extension HomeViewController: HomeViewable {
    func showMessage(text: String) {
        let alert = UIAlertController(title: "error_message".localized, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok_message".localized, style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showMovies() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
        }
    }
}

extension HomeViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationController.Operation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return AnimationManager(animationDuration: 0.75, animationType: .present)
        case .pop:
            return AnimationManager(animationDuration: 0.75, animationType: .dismiss)
        default:
            return nil
        }
    }
}

protocol MovieTransitionable {
    var backgroundView: UIView { get }
    var movieImage: UIImageView { get }
}

extension HomeViewController: MovieTransitionable {
    var backgroundView: UIView {
        guard
            let indexPath = selectedIndexPath,
            let movieCell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell
        else {
            return UIView()
        }
        return movieCell.contentView
    }

    var movieImage: UIImageView {
        guard
            let indexPath = selectedIndexPath,
            let movieCell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell
        else {
            return UIImageView()
        }
        return movieCell.movieImageView
    }
}

