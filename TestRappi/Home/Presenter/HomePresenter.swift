//
//  HomePresenter.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import Foundation


protocol HomeViewable: NSObjectProtocol {
    func showMovies()
    func showMessage(text: String)
}

class HomePresenter: NSObject {
    weak var viewable: HomeViewable?
    private let router: HomeRouter
    private let interactor: HomeInteractor
    var filteredMovies: [Movie]? {
        didSet {
            viewable?.showMovies()
        }
    }
    var movies: [Movie]?
    
    init(viewable: HomeViewable, router: HomeRouter, interactor: HomeInteractor) {
        self.viewable = viewable
        self.router = router
        self.interactor = interactor
    }
    
    func getMovies() {
        interactor.requestMoviesFromService()
    }
    
    func filterMovies(title: String) {
        guard !title.isEmpty else {
            filteredMovies = movies
            return
        }
        filteredMovies = movies?.filter({ movie -> Bool in
            return movie.title.lowercased().contains(title.lowercased())
        })
    }
    
    func goToMovieDetail(indexPath: IndexPath) {
        guard let movie = filteredMovies?[indexPath.row] else { return }
        router.goToMovieDetail(movie: movie)
    }
}

extension HomePresenter: HomeInteractorOutput {
    func handlerError(error: Error) {
        if let error = error as? URLError {
            if error.code.rawValue == NSURLErrorNotConnectedToInternet {
                viewable?.showMessage(text: "connection_error".localized)
            }
        }
        if let error = error as? RequestResponseError {
            switch error {
            case .connectivity:
                viewable?.showMessage(text: "connection_error".localized)
            case .emptyData:
                viewable?.showMessage(text: "data_error".localized)
            case .unexpected:
                viewable?.showMessage(text: "unexpected".localized)
            }
        }
    }
    
    func moviesList(movies: [Movie]) {
        self.filteredMovies = movies
        self.movies = movies
    }
}
