//
//  StringExtension.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 16/07/21.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
