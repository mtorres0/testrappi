//
//  ImageCache.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 16/07/21.
//

import UIKit

class ImageCache {
    static let shared = ImageCache()
    let cache = NSCache<NSString,UIImage>()
    private init() {}
}
