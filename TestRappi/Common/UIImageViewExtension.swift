//
//  UIImageViewExtension.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import UIKit

extension UIImageView {
    
    /// This method load a image from a url
    /// 
    /// - parameters:
    ///     -   urlString: The image url string
    ///     -   placeholder: A default image
    func imageFromURL(urlString: String, placeholder: UIImage) {
        if self.image == nil {
            self.image = placeholder
        }
        if let image = ImageCache.shared.cache.object(forKey: urlString as NSString) {
            self.image = image
        }
        guard let urlImage = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: urlImage) { (data, urlResponse, error) in
            guard let data = data, error == nil, let image = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                ImageCache.shared.cache.setObject(image, forKey: urlString as NSString)
                self.image = image
            }
        }.resume()
    }
}
