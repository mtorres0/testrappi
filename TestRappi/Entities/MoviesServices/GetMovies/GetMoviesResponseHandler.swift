//
//  GetMoviesResponseHandler.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

class GetMoviesResponseHandler: ResponseHandler {
    private let completion: (Result<MoviesSuccessResponse, Error>) -> Void
    
    init(completion: @escaping (Result<MoviesSuccessResponse, Error>) -> Void) {
        self.completion = completion
    }
    
    func parseResponse(data: Data) {
      // Handle Error Response
      if let errorResponse = try? JSONDecoder().decode(MoviesErrorResponse.self, from: data) {
        completion(.failure(ResponseHandlerError.errorMessage(errorResponse.statusMessage)))
        return
      }

      do {
        // Handle Success Response
        let response = try JSONDecoder().decode(MoviesSuccessResponse.self, from: data)
        completion(.success(response))
      } catch let error {
        debugPrint("Error parse MoviesSuccessResponse: \(error.localizedDescription)")
        completion(.failure(ResponseHandlerError.invalidData))
      }
    }
    
    func response(error: Error) {
        completion(.failure(error))
    }
}
