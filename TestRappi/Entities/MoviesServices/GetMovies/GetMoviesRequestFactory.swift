//
//  GetMoviesRequestFactory.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

class GetMoviesRequestFactory: RequestFactory {
    var path: String {
        return self.endpoint
    }
    
    private let baseURL: URL
    private let parameters: [ParameterName: String]
    private let endpoint: String
    
    init(baseURL: URL, parameters: [ParameterName: String], endpoint: String) {
        self.baseURL = baseURL
        self.parameters = parameters
        self.endpoint = endpoint
    }
    
    func makeRequest() throws -> URLRequest {
        // Set URL
        var components = URLComponents(string: self.baseURL.absoluteString)!
        components.path = path
        components.queryItems = try queryItems(parameters: self.parameters)

        // Check for invalid URL
        guard let url = components.url else { throw RequestFactoryError.invalidURL }
        let request = URLRequest(url: url)
        
        return request
    }
}

extension GetMoviesRequestFactory: RequestParameters {
    
    func queryItems(parameters: [ParameterName: String]) throws -> [URLQueryItem] {
        // Set Required Parameters
        var queryItems = [
            URLQueryItem(name: ParameterName.apiKey.rawValue, value: parameters[.apiKey]),
        ]
        
        // Check for Missing Required Parameter
        if queryItems.contains(where: { $0.value?.isEmpty ?? true }) {
            throw RequestFactoryError.missingParameter
        }
        
        // Set Optional Parameters
        if let language = parameters[.language] {
            queryItems.append(
                URLQueryItem(name: ParameterName.language.rawValue, value: language)
            )
        }
        if let page = parameters[.page] {
            queryItems.append(
                URLQueryItem(name: ParameterName.page.rawValue, value: page)
            )
        }
        if let region = parameters[.language] {
            queryItems.append(
                URLQueryItem(name: ParameterName.region.rawValue, value: region)
            )
        }
        
        return queryItems
    }
}
