//
//  MoviesSuccessResponse.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

struct MoviesSuccessResponse {
    let page: Int
    let results:[Movie]
}

extension MoviesSuccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case page
        case results
    }
}
