//
//  MoviesErrorResponse.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

struct MoviesErrorResponse {
    let statusMessage: String
    let success: Bool
    let statusCode: Int
}

extension MoviesErrorResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case statusMessage = "status_message"
        case success
        case statusCode = "status_code"
    }
}
