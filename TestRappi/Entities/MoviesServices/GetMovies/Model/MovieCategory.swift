//
//  MovieCategory.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

enum MovieCategory: String {
    case popular
    case topRated
    case upcoming
}
