//
//  Movie.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

struct Movie {
    let id: Int
    let title: String
    let overview: String
    var releaseDate: String?
    let imagePath: String
}

extension Movie: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case releaseDate = "release_date"
        case imagePath = "poster_path"
    }
}
