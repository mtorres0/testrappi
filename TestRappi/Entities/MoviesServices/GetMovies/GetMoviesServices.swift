//
//  GetMoviesServices.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

struct GetMoviesServices {
    private let baseURL: URL
    private let parameters: [ParameterName: String]
    private let category: MovieCategory
    
    init(baseURL: URL, parameters: [ParameterName: String], category: MovieCategory) {
        self.baseURL = baseURL
        self.parameters = parameters
        self.category = category
    }
    
    func getMovies(_ completion: @escaping (Result<MoviesSuccessResponse,Error>) -> Void) {
        do {
            let requestFactory = GetMoviesRequestFactory(
                baseURL: self.baseURL,
                parameters: self.parameters,
                endpoint: getEndpointBy(category: category))
            let request = try requestFactory.makeRequest()
            
            let responder = GetMoviesResponseHandler(completion: completion)
            
            let cache = URLCache(memoryCapacity: 0, diskCapacity: 100 * 1024 * 1024, diskPath: "myCache")
            
            let config = URLSessionConfiguration.default
            config.urlCache = cache
            config.requestCachePolicy = .returnCacheDataElseLoad
            let session = URLSession(configuration: config)
            
            session.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    responder.response(error: error)
                } else if let data = data, let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200...299:
                        responder.parseResponse(data: data)
                    default:
                        responder.response(error: RequestResponseError.connectivity)
                    }
                } else {
                    responder.response(error: RequestResponseError.unexpected)
                }
            }).resume()
        } catch {
            completion(.failure(error))
        }
    }
    
    private func getEndpointBy(category: MovieCategory) -> String {
        switch category {
        case .popular:
            return ApiConstants.MoviesEndpoints.getPopular
        case .topRated:
            return ApiConstants.MoviesEndpoints.getTopRated
        case .upcoming:
            return ApiConstants.MoviesEndpoints.getUpcoming
        }
    }
}
