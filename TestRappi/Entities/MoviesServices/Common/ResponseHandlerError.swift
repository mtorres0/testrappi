//
//  ResponseHandlerError.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

enum ResponseHandlerError: Error, Equatable {
    case invalidData
    case errorMessage(String)
}
