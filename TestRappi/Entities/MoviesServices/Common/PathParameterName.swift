//
//  PathParameterName.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

enum PathParameterName: String, Hashable {
    case apiKey = "api_key"
    case language
    case page
    case region
}
