//
//  RequestParameters.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

/// Defines methods for Parameters compatibility.
/// Concrete Request Factories can implement it to add query Parameters compatiblity.
protocol RequestParameters {

  /// Make an Array of `URLQueryItem` based on the received `Parameters`.
  /// - Parameter parameters: A `Parameters` instance.
    func queryItems(parameters: [ParameterName: String]) throws -> [URLQueryItem]
}
