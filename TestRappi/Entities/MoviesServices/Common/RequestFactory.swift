//
//  RequestFactory.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

/// Defines properties and methods that a `RequestFactory` implementation must comply
/// in order to generate `URLRequest` that will be performed by the `APIClient`.
public protocol RequestFactory {

  /// Defines the path of the URL.
  var path: String { get }

  /// Method to set up a `URLRequest` based on the `ServiceContext` dependency.
  func makeRequest() throws -> URLRequest
}
