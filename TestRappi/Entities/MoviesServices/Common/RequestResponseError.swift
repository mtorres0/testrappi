//
//  RequestResponseError.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

public enum RequestResponseError: Error {
  case emptyData
  case connectivity
  case unexpected
}
