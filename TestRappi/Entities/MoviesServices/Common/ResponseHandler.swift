//
//  ResponseHandler.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

protocol ResponseHandler {
    /// Method to receives the `Data` to be parsed.
    /// - Parameter data: The `Data` to be parsed.
    func parseResponse(data: Data)
    
    /// Method to handle or completes with recived error.
    /// - Parameter error: An `Error` subtype.
    func response(error: Error)
}
