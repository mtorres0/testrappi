//
//  RequestFactoryError.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

enum RequestFactoryError: Error {
  case missingParameter
  case invalidURL
}
