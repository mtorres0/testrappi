//
//  RequestPathParameters.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

/// Defines methods for PathParameters compatibility.
/// Concrete Request Factories can implement it to add `PathParameters` compatiblity.
protocol RequestPathParameters {

  /// Make an absolutePath String based on the received `PathParameters`.
  /// - Parameter parameters: A `PathParameters` instance.
  func absolutePath( parameters: [PathParameterName : String]) throws -> String
}
