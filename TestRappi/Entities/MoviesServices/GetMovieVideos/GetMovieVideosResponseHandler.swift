//
//  GetMovieVideosResponseHandler.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

class GetMovieVideosResponseHandler: ResponseHandler {
    private let completion: (Result<MovieVideosSuccessResponse, Error>) -> Void
    
    init(completion: @escaping (Result<MovieVideosSuccessResponse, Error>) -> Void) {
        self.completion = completion
    }
    
    func parseResponse(data: Data) {
      // Handle Error Response
      if let errorResponse = try? JSONDecoder().decode(MoviesErrorResponse.self, from: data) {
        completion(.failure(ResponseHandlerError.errorMessage(errorResponse.statusMessage)))
        return
      }

      do {
        // Handle Success Response
        let response = try JSONDecoder().decode(MovieVideosSuccessResponse.self, from: data)
        completion(.success(response))
      } catch let error {
        debugPrint("JSONDecoder Error: \(error.localizedDescription)")
        completion(.failure(ResponseHandlerError.invalidData))
      }
    }
    
    func response(error: Error) {
        completion(.failure(error))
    }
}
