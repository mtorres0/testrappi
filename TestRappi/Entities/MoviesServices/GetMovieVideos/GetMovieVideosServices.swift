//
//  GetMovieVideosServices.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

struct GetMovieVideosServices {
    private let baseURL: URL
    private let parameters: [ParameterName: String]
    
    init(baseURL: URL, parameters: [ParameterName: String]) {
        self.baseURL = baseURL
        self.parameters = parameters
    }
    
    func getMovies(movieId: Int, _ completion: @escaping (Result<MovieVideosSuccessResponse,Error>) -> Void) {
        do {
            let requestFactory = GetMovieVideosRequestFactory(
                movieId: movieId,
                baseURL: self.baseURL,
                parameters: self.parameters)
            let request = try requestFactory.makeRequest()
            
            let responder = GetMovieVideosResponseHandler(completion: completion)
            
            let cache = URLCache(memoryCapacity: 0, diskCapacity: 100 * 1024 * 1024, diskPath: "myCache")
            
            let config = URLSessionConfiguration.default
            config.urlCache = cache
            config.requestCachePolicy = .returnCacheDataElseLoad
            let session = URLSession(configuration: config)
            
            session.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    responder.response(error: error)
                } else if let data = data, let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200...299:
                        responder.parseResponse(data: data)
                    default:
                        responder.response(error: RequestResponseError.connectivity)
                    }
                } else {
                    responder.response(error: RequestResponseError.unexpected)
                }
            }).resume()
        } catch {
            completion(.failure(error))
        }
    }
}
