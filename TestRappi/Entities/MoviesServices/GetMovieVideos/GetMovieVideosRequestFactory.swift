//
//  GetMovieVideosRequestFactory.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 18/07/21.
//

import Foundation

class GetMovieVideosRequestFactory: RequestFactory {
    var path: String {
        return ApiConstants.MoviesEndpoints.getMovieVideos
    }
    
    private let baseURL: URL
    private let parameters: [ParameterName: String]
    private let movieId: Int
    
    init(movieId: Int, baseURL: URL, parameters: [ParameterName: String]) {
        self.movieId = movieId
        self.baseURL = baseURL
        self.parameters = parameters
    }
    
    func makeRequest() throws -> URLRequest {
        // Set URL
        var components = URLComponents(string: self.baseURL.absoluteString)!
        components.path = path
        components.queryItems = try queryItems(parameters: self.parameters)
        
        // add movie id
        components.path = components.path.replacingOccurrences(of: "{movie_id}", with: "\(movieId)")

        // Check for invalid URL
        guard let url = components.url else { throw RequestFactoryError.invalidURL }
        let request = URLRequest(url: url)
        
        return request
    }
}

extension GetMovieVideosRequestFactory: RequestParameters {
    
    func queryItems(parameters: [ParameterName: String]) throws -> [URLQueryItem] {
        // Set Required Parameters
        var queryItems = [
            URLQueryItem(name: ParameterName.apiKey.rawValue, value: parameters[.apiKey]),
        ]
        
        // Check for Missing Required Parameter
        if queryItems.contains(where: { $0.value?.isEmpty ?? true }) {
            throw RequestFactoryError.missingParameter
        }
        
        // Set Optional Parameters
        if let language = parameters[.language] {
            queryItems.append(
                URLQueryItem(name: ParameterName.language.rawValue, value: language)
            )
        }
        
        return queryItems
    }
}
