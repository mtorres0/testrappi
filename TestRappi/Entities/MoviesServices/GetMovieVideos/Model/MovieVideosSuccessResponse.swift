//
//  MovieVideosSuccessResponse.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

struct MovieVideosSuccessResponse {
    let id: Int
    let videos: [MovieVideo]
}

extension MovieVideosSuccessResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case videos = "results"
    }
}
