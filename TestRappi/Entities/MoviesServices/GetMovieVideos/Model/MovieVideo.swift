//
//  MovieVideo.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

struct MovieVideo {
    let id: String
    let iso639: String
    let iso3166: String
    let key: String
    let name: String
    let site: String
    let size: Int
    let type: String
}

extension MovieVideo: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case iso639 = "iso_639_1"
        case iso3166 = "iso_3166_1"
        case key
        case name
        case site
        case size
        case type
    }
}
