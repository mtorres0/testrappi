//
//  ApiContants.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 13/07/21.
//

import Foundation

struct ApiConstants {
    // the api key to use the movie db API.
    static let apiKey = "e8a02e251af6d1c43346b9b90fc21905"
    // the base url for all api methods
    static let baseURL = "https://api.themoviedb.org"
    // the base url for images
    static let baseImageURL = "https://image.tmdb.org/t/p/w500"
    
    
    // api methods for movies
    struct MoviesEndpoints {
        static let getPopular = "/3/movie/popular"
        static let getTopRated = "/3/movie/top_rated"
        static let getUpcoming = "/3/movie/upcoming"
        static let getMovieVideos = "/3/movie/{movie_id}/videos"
    }
}
