//
//  TabBarControllerFactory.swift
//  TestRappi
//
//  Created by Michel Torres Alonso on 16/07/21.
//

import UIKit

class TabBarControllerFactory {
    static func createTabBar() -> UITabBarController {
        let popularItem = UITabBarItem()
        popularItem.title = "popular_movies".localized
        popularItem.image = UIImage(systemName: "star.fill")
        let popularMoviesViewController = HomeRouter.createHomeViewController(category: .popular)
        popularMoviesViewController.tabBarItem = popularItem    
        
        let topRatedItem = UITabBarItem()
        topRatedItem.title = "top_rated_movies".localized
        topRatedItem.image = UIImage(systemName: "arrow.up.to.line")
        let topRatedMoviesViewController = HomeRouter.createHomeViewController(category: .topRated)
        topRatedMoviesViewController.tabBarItem = topRatedItem
        
        let upcomingItem = UITabBarItem()
        upcomingItem.title = "uncoming_movies".localized
        upcomingItem.image = UIImage(systemName: "calendar.badge.plus")
        let upcomingMoviesViewController = HomeRouter.createHomeViewController(category: .upcoming)
        upcomingMoviesViewController.tabBarItem = upcomingItem
        
        let tabBarController = TabBarViewController()
        tabBarController.viewControllers = [
            popularMoviesViewController,
            topRatedMoviesViewController,
            upcomingMoviesViewController
        ]
        return tabBarController
    }
}
