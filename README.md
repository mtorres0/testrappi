# README #

La aplicación fue implementada siguiendo el patrón VIPER, adjunto una imagen del diagrama. 

![Diagrama](TestRappi.jpg)

### Una vez acabada la prueba describa en un readme brevemente: ###

Tal cual está definido VIPER, la responsabilidad de cada elemento es la siguiente: 

* View: Encargada de todos los cambios que surgan en la vista y de las interacciones del usuario.

* Interactor: Encargado de la lógica de negoocio y casos de uso. 

* Presenter: Encargado de propagar las acciones del usuario y de procesar la info para poder presentarsela al usuario. 

* Entities: Todos los elementos que son el origen de nuestros datos. 

* Router: El encargado de la navegación. 

### Una vez acabada la prueba describa en un readme brevemente: ###

* ¿En qué consiste el principio de responsabilidad única? 

Consiste en que cada elemento de nuestro código debe tener un solo proposito. Por ejemplo, una vista (View) tierne definida la responsabilidad de actualizar los elementos visuales y las interacciones del usuario. Si dicha vista tiene la llamada a un servicio dentro de su lógica, estaríamos violando este principio ya que no es su responsabilidad. 

* Qué características tiene, según su opinión, un “buen” código o código limpio.

Un “buen” código o código limpio se refiere a que debemos generar un código que sea entendible y mantenible en la mayor medida posible. Esto puede ser desde la forma en que llamamos una variable o función, en donde debemos de hacerlo de forma clara y sencilla para que se entienda su proposito. El agregar los comentarios pertinentes dentro del código. Y también esto puede incluir el uso de test y patrones que nos permitan que nuestro código pueda ser mantenible de una forma más sencilla. 