//
//  GetMovieVideosResponseHandlerTestsHelper.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 19/07/21.
//

import Foundation

struct GetMovieVideosResponseHandlerTestsHelper {
    static func makeErrorResponse() -> [String:Any] {
        let json: [String: Any] = [
            "status_message": "Invalid API key: You must be granted a valid key.",
            "success": false,
            "status_code": 7
        ]
        return json
    }
    
    static func makeSuccessResponse() -> [String:Any] {
        let json: [String: Any] = [
            "id": 550,
            "results": [
                [
                    "id": "533ec654c3a36854480003eb",
                    "iso_639_1": "en",
                    "iso_3166_1": "US",
                    "key": "SUXWAEX2jlg",
                    "name": "Trailer 1",
                    "site": "YouTube",
                    "size": 720,
                    "type": "Trailer"
                ]
            ]
        ]
        return json
    }
}
