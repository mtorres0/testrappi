//
//  GetMovieVideosRequestFactoryTestsHelper.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 18/07/21.
//

import Foundation
@testable import TestRappi

struct GetMovieVideosRequestFactoryTestsHelper {
    static func queryString(parameters: [ParameterName: String]) -> String {
        var query = "\(ParameterName.apiKey.rawValue)=\(parameters[.apiKey] ?? "")"
        if let language = parameters[.language] {
            query += "&\(ParameterName.language.rawValue)=\(language)"
        }
        return query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
}
