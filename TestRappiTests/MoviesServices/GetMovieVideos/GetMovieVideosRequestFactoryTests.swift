//
//  GetMovieVideosRequestFactoryTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 18/07/21.
//

import XCTest
@testable import TestRappi


class GetMovieVideosRequestFactoryTests: XCTestCase {

    func test_makeRequest_throwsError_missingRequiredParameter_whenSetEmptyParameters() throws {
        let parameters = [ParameterName: String]()
        let baseURL = URL(string: ApiConstants.baseURL)!
        let movieId = 497698
        let sut = GetMovieVideosRequestFactory(movieId: movieId, baseURL: baseURL, parameters: parameters)
        
        XCTAssertThrowsError(try sut.makeRequest()) { error in
            XCTAssertEqual(error as? RequestFactoryError, RequestFactoryError.missingParameter)
        }
    }
    
    func test_makeRequest_verifyAllRequiredParameters() {
        var parameters = [ParameterName: String]()
        parameters = [ParameterName.apiKey: ApiConstants.apiKey]
        let baseURL = URL(string: ApiConstants.baseURL)!
        let movieId = 497698
        let endpoint = ApiConstants.MoviesEndpoints.getMovieVideos.replacingOccurrences(of: "{movie_id}", with: "\(movieId)")
        let sut = GetMovieVideosRequestFactory(movieId: movieId, baseURL: baseURL, parameters: parameters)
        
        let sutURLRequest = try? sut.makeRequest()
        
        XCTAssertEqual(sutURLRequest?.url?.host, baseURL.host)
        XCTAssertEqual(sutURLRequest?.url?.path, endpoint)
        XCTAssertEqual(sutURLRequest?.url?.query, "\(GetMoviesRequestFactoryTestsHelper.queryString(parameters: parameters))")
    }
    
    func test_makeRequest_verifyAllOptionalParameters() {
        var parameters = [ParameterName: String]()
        parameters[ParameterName.apiKey] = ApiConstants.apiKey
        parameters[ParameterName.language] = "en-US"
        let baseURL = URL(string: ApiConstants.baseURL)!
        let movieId = 497698
        let sut = GetMovieVideosRequestFactory(movieId: movieId, baseURL: baseURL, parameters: parameters)
        
        let sutURLRequest = try? sut.makeRequest()
        
        XCTAssertEqual(sutURLRequest?.url?.host, baseURL.host)
        XCTAssertEqual(sutURLRequest?.url?.query, "\(GetMovieVideosRequestFactoryTestsHelper.queryString(parameters: parameters))")
    }

}
