//
//  GetMovieVideosResponseHandlerTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 18/07/21.
//

import XCTest
@testable import TestRappi

class GetMovieVideosResponseHandlerTests: XCTestCase {

    func test_request_throwError_invalidData() throws {
        let fakeResponse = APICompletionStub<MovieVideosSuccessResponse>()
        let wrongData = "wrong data".data(using: .utf8)!
        let sut = GetMovieVideosResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: wrongData)
        
        XCTAssertEqual(fakeResponse.errorResponse as? ResponseHandlerError, ResponseHandlerError.invalidData)
    }
    
    func test_requestReturnError() throws {
        let fakeResponse = APICompletionStub<MovieVideosSuccessResponse>()
        let jsonError = GetMovieVideosResponseHandlerTestsHelper.makeErrorResponse()
        let data = try JSONSerialization.data(withJSONObject: jsonError)
        let sut = GetMovieVideosResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: data)
        
        XCTAssertEqual(fakeResponse.errorResponse as? ResponseHandlerError, ResponseHandlerError.errorMessage(jsonError["status_message"] as! String))
    }
    
    func test_requestReturnSuccessResponse() throws {
        let fakeResponse = APICompletionStub<MovieVideosSuccessResponse>()
        let jsonSuccess = GetMovieVideosResponseHandlerTestsHelper.makeSuccessResponse()
        let data = try JSONSerialization.data(withJSONObject: jsonSuccess)
        let sut = GetMovieVideosResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: data)
        
        XCTAssertEqual(fakeResponse.successResponse?.id, jsonSuccess["id"] as? Int )
        let movieVideos = jsonSuccess["results"] as! [[String:Any]]
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.id, movieVideos.first?["id"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.iso639, movieVideos.first?["iso_639_1"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.iso3166, movieVideos.first?["iso_3166_1"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.key, movieVideos.first?["key"] as? String)
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.name, movieVideos.first?["name"] as? String)
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.site, movieVideos.first?["site"] as? String)
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.size, movieVideos.first?["size"] as? Int)
        XCTAssertEqual(fakeResponse.successResponse?.videos.first?.type, movieVideos.first?["type"] as? String)
    }

}
