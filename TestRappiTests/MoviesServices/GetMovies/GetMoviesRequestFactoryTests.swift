//
//  GetMoviesRequestFactoryTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 13/07/21.
//

import XCTest
@testable import TestRappi

class GetMoviesRequestFactoryTests: XCTestCase {
    
    func test_makeRequest_throwsError_missingRequiredParameter_whenSetEmptyParameters() throws {
        let parameters = [ParameterName: String]()
        let baseURL = URL(string: ApiConstants.baseURL)!
        let sut = GetMoviesRequestFactory(baseURL: baseURL, parameters: parameters, endpoint: ApiConstants.MoviesEndpoints.getPopular)
        
        XCTAssertThrowsError(try sut.makeRequest()) { error in
            XCTAssertEqual(error as? RequestFactoryError, RequestFactoryError.missingParameter)
        }
    }
    
    func test_makeRequest_verifyAllRequiredParameters() {
        var parameters = [ParameterName: String]()
        parameters = [ParameterName.apiKey: ApiConstants.apiKey]
        let baseURL = URL(string: ApiConstants.baseURL)!
        let endpoint = ApiConstants.MoviesEndpoints.getPopular
        let sut = GetMoviesRequestFactory(baseURL: baseURL, parameters: parameters, endpoint: endpoint)
        
        let sutURLRequest = try? sut.makeRequest()
        
        XCTAssertEqual(sutURLRequest?.url?.host, baseURL.host)
        XCTAssertEqual(sutURLRequest?.url?.path, endpoint)
        XCTAssertEqual(sutURLRequest?.url?.query, "\(GetMoviesRequestFactoryTestsHelper.queryString(parameters: parameters))")
    }
    
    func test_makeRequest_verifyAllOptionalParameters() {
        var parameters = [ParameterName: String]()
        parameters[ParameterName.apiKey] = ApiConstants.apiKey
        parameters[ParameterName.language] = "en-US"
        parameters[ParameterName.page] = "1"
        parameters[ParameterName.region] = "MX"
        let baseURL = URL(string: ApiConstants.baseURL)!
        let sut = GetMoviesRequestFactory(baseURL: baseURL, parameters: parameters, endpoint: ApiConstants.MoviesEndpoints.getPopular)
        
        let sutURLRequest = try? sut.makeRequest()
        
        XCTAssertEqual(sutURLRequest?.url?.host, baseURL.host)
        XCTAssertEqual(sutURLRequest?.url?.query, "\(GetMoviesRequestFactoryTestsHelper.queryString(parameters: parameters))")
    }
}
