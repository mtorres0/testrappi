//
//  GetMoviesResponseHandlerTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import XCTest
@testable import TestRappi

class GetMoviesResponseHandlerTests: XCTestCase {

    func test_request_throwError_invalidData() throws {
        let fakeResponse = APICompletionStub<MoviesSuccessResponse>()
        let wrongData = "wrong data".data(using: .utf8)!
        let sut = GetMoviesResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: wrongData)
        
        XCTAssertEqual(fakeResponse.errorResponse as? ResponseHandlerError, ResponseHandlerError.invalidData)
    }
    
    func test_requestReturnError() throws {
        let fakeResponse = APICompletionStub<MoviesSuccessResponse>()
        let jsonError = GetMoviesResponseHandlerTestsHelper.makeErrorResponse()
        let data = try JSONSerialization.data(withJSONObject: jsonError)
        let sut = GetMoviesResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: data)
        
        XCTAssertEqual(fakeResponse.errorResponse as? ResponseHandlerError, ResponseHandlerError.errorMessage(jsonError["status_message"] as! String))
    }
    
    func test_requestReturnSuccessResponse() throws {
        let fakeResponse = APICompletionStub<MoviesSuccessResponse>()
        let jsonSuccess = GetMoviesResponseHandlerTestsHelper.makeSuccessResponse()
        let data = try JSONSerialization.data(withJSONObject: jsonSuccess)
        let sut = GetMoviesResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: data)
        
        XCTAssertEqual(fakeResponse.successResponse?.page, jsonSuccess["page"] as? Int )
        let movies = jsonSuccess["results"] as! [[String:Any]]
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.title, movies.first?["title"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.id, movies.first?["id"] as? Int )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.overview, movies.first?["overview"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.releaseDate, movies.first?["release_date"] as? String)
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.imagePath, movies.first?["poster_path"] as? String)
    }
    
    func test_requestReturnSuccessResponseWithoutReleaseDate() throws {
        let fakeResponse = APICompletionStub<MoviesSuccessResponse>()
        let jsonSuccess = GetMoviesResponseHandlerTestsHelper.makeSuccessResponseWithoutRealeaseDate()
        let data = try JSONSerialization.data(withJSONObject: jsonSuccess)
        let sut = GetMoviesResponseHandler(completion: fakeResponse.completion)
        
        sut.parseResponse(data: data)
        
        XCTAssertEqual(fakeResponse.successResponse?.page, jsonSuccess["page"] as? Int )
        let movies = jsonSuccess["results"] as! [[String:Any]]
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.title, movies.first?["title"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.id, movies.first?["id"] as? Int )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.overview, movies.first?["overview"] as? String )
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.releaseDate, movies.first?["release_date"] as? String)
        XCTAssertEqual(fakeResponse.successResponse?.results.first?.imagePath, movies.first?["poster_path"] as? String)
    }
}
