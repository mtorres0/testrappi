//
//  GetMoviesResponseHandlerTestsHelper.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation

class APICompletionStub<T> {
    var completion: ((Result<T, Error>) -> Void)!
    var errorResponse: Error? { return error }
    var successResponse: T? { return value}
    
    private var value: T?
    private var error: Error?
    
    init() {
        completion = { result in
            switch result {
            case .failure(let error):
                self.error = error
            case .success(let successResponse):
                self.value = successResponse
            }
        }
    }
}

struct GetMoviesResponseHandlerTestsHelper {
    static func makeErrorResponse() -> [String:Any] {
        let json: [String: Any] = [
            "status_message": "Invalid API key: You must be granted a valid key.",
            "success": false,
            "status_code": 7
        ]
        return json
    }
    
    static func makeSuccessResponse() -> [String:Any] {
        let json: [String: Any] = [
            "page": 1,
            "results": [
                [
                "poster_path": "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg",
                "adult": false,
                "overview": "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                "release_date": "2016-08-03",
                "genre_ids": [
                14,
                28,
                80
                ],
                "id": 297761,
                "original_title": "Suicide Squad",
                "original_language": "en",
                "title": "Suicide Squad",
                "backdrop_path": "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg",
                "popularity": 48.261451,
                "vote_count": 1466,
                "video": false,
                "vote_average": 5.91
                ]
            ]
        ]
        return json
    }
    
    static func makeSuccessResponseWithoutRealeaseDate() -> [String:Any] {
        let json: [String: Any] = [
            "page": 1,
            "results": [
                [
                    "adult":false,
                    "backdrop_path":nil,
                    "genre_ids":[
                    28,
                    80
                    ],
                    "id":385687,
                    "original_language":"en",
                    "original_title":"Fast & Furious 10",
                    "overview":"The tenth installment in the Fast Saga.",
                    "popularity":1357.871,
                    "poster_path":"/2DyEk84XnbJEdPlGF43crxfdtHH.jpg",
                    "title":"Fast & Furious 10",
                    "video":false,
                    "vote_average":0,
                    "vote_count":0
                ]
            ]
        ]
        return json
    }
}
