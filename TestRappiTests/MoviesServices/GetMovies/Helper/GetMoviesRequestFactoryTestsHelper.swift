//
//  GetMoviesRequestFactoryTestsHelper.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 14/07/21.
//

import Foundation
@testable import TestRappi

struct GetMoviesRequestFactoryTestsHelper {
    static func queryString(parameters: [ParameterName: String]) -> String {
        var query = "\(ParameterName.apiKey.rawValue)=\(parameters[.apiKey] ?? "")"
        if let language = parameters[.language] {
            query += "&\(ParameterName.language.rawValue)=\(language)"
        }
        if let page = parameters[.page] {
            query += "&\(ParameterName.page.rawValue)=\(page)"
        }
        if let region = parameters[.language] {
            query += "&\(ParameterName.region.rawValue)=\(region)"
        }
        return query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
}
