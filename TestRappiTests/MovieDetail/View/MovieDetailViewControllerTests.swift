//
//  MovieDetailViewControllerTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 16/07/21.
//

import XCTest
@testable import TestRappi

class MovieDetailViewControllerTests: XCTestCase {


    func testMovieDetailShowAllInfo() throws {
        let movie = Movie(
            id: 1,
            title: "The Light Between Oceans",
            overview: "A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from an adrift rowboat.",
            releaseDate: "2016-09-02",
            imagePath: "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg")
        let sut = MovieDetailRouter.createMovieDetailViewController(movie: movie) as! MovieDetailViewController
        
        _ = sut.view
        
        XCTAssertEqual(sut.movieTitleLabel.text, movie.title)
        XCTAssertEqual(sut.releaseDateLabel.text, movie.releaseDate)
        XCTAssertEqual(sut.overviewTextView.text, movie.overview)
    }

}
