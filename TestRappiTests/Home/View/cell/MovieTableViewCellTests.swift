//
//  MovieTableViewCellTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import XCTest
@testable import TestRappi

class FakeViewController: UIViewController, UITableViewDataSource {
    
    var tableView = UITableView()
    private let movies: [Movie]
    init(movies: [Movie]) {
        self.movies = movies
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell") as! MovieTableViewCell
        cell.setup(movie: movies[indexPath.row])
        return cell
    }
}

class MovieTableViewCellTests: XCTestCase {
    
    func testCellSetup() throws {
        let movie = Movie(
            id: 1,
            title: "The Light Between Oceans",
            overview: "A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from an adrift rowboat.",
            releaseDate: "2016-09-02",
            imagePath: "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg")
        let sut = FakeViewController(movies: [movie])
        _ = sut.view
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = sut.tableView.dataSource?.tableView(sut.tableView, cellForRowAt: indexPath) as! MovieTableViewCell

        XCTAssertNotNil(cell)
        XCTAssertEqual(cell.titleLabel.text, movie.title)
        XCTAssertEqual(cell.releaseDateLabel.text, movie.releaseDate)
        XCTAssertEqual(cell.overviewTextView.text, movie.overview)
    }

}
