//
//  HomeViewControllerTests.swift
//  TestRappiTests
//
//  Created by Michel Torres Alonso on 15/07/21.
//

import XCTest
@testable import TestRappi


class FakePresenter: HomePresenter {
    init(viewable: HomeViewable, router: HomeRouter, interactor: HomeInteractor, movies: [Movie]) {
        super.init(viewable: viewable, router: router, interactor: interactor)
        self.movies = movies
        self.filteredMovies = movies
    }
}

class HomeViewControllerTests: XCTestCase {

    func test_tableViewInNotNil() throws {
        let sut = makeSUT()
        
        _ = sut.view
        
        XCTAssertNotNil(sut.tableView)
    }
    
    func test_moviesAreShownInTableView() {
        let movie = Movie(
            id: 1,
            title: "The Light Between Oceans",
            overview: "A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from an adrift rowboat.",
            releaseDate: "2016-09-02",
            imagePath: "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg")
        let listMovies = [movie, movie]
        
        let sut = makeSUT(listMovies: listMovies)
        _ = sut.view
        
        XCTAssertEqual(sut.tableView.numberOfRows(inSection: 0), listMovies.count)
    }
    
    func test_searchAMovieIsInTheList() {
        let movie = Movie(
            id: 1,
            title: "Jason Bourne",
            overview: "The most dangerous former operative of the CIA is drawn out of hiding to uncover hidden truths about his past.",
            releaseDate: "2016-07-27",
            imagePath: "/lFSSLTlFozwpaGlO31OoUeirBgQ.jpg")
        let movie2 = Movie(
            id: 1,
            title: "The Light Between Oceans",
            overview: "A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from an adrift rowboat.",
            releaseDate: "2016-09-02",
            imagePath: "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg")
        let listMovies = [movie, movie2]
        let sut = makeSUT(listMovies: listMovies)
        
        sut.presenter?.filterMovies(title: "The Light")
        
        XCTAssertEqual(sut.presenter?.filteredMovies?.first?.title, movie2.title)
    }
    
    func test_searchAMovieIsNotInTheList() {
        let movie = Movie(
            id: 1,
            title: "Jason Bourne",
            overview: "The most dangerous former operative of the CIA is drawn out of hiding to uncover hidden truths about his past.",
            releaseDate: "2016-07-27",
            imagePath: "/lFSSLTlFozwpaGlO31OoUeirBgQ.jpg")
        let movie2 = Movie(
            id: 1,
            title: "The Light Between Oceans",
            overview: "A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from an adrift rowboat.",
            releaseDate: "2016-09-02",
            imagePath: "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg")
        let listMovies = [movie, movie2]
        let sut = makeSUT(listMovies: listMovies)

        sut.presenter?.filterMovies(title: "Ju")
        
        XCTAssertEqual(sut.presenter?.filteredMovies?.count, 0)
    }

}

extension HomeViewControllerTests {
    func makeSUT(listMovies: [Movie] = []) -> HomeViewController{
        let sut = HomeViewController()
        let urlMovies = URL(string: ApiConstants.baseURL)!
        let parameters = [
            ParameterName.apiKey: ApiConstants.apiKey
        ]
        let moviesServices = GetMoviesServices(baseURL: urlMovies, parameters: parameters, category: .popular)
        let fakePresenter = FakePresenter(viewable: sut, router: HomeRouter(), interactor: HomeInteractor(moviesServices: moviesServices), movies: listMovies)
        sut.presenter = fakePresenter
        return sut
    }
}
